## Desafio Final Data Science
#### Descripción del trabajo:
El objetivo del trabajo se oriento en predecir la asistencia de pacientes a un turno médico obtenido previamente.

Requerimientos:
* Tener instalado git, docker y docker-compose instalado y tener un cliente REST como Postman para probar directamente la API

#### Pasos para correr la prueba:
* Clonar el repositorio

		$ git clone https://gitlab.com/lvillar/desafioFinal.git

* Buildear las imagenes de Docker.

		$ docker-compose build
		
* Ejecutar el docker-compose.yml para levantar el contenedor.

		$ docker-compose up

### Modos de consultar el modelo predictivo
* Acceder al webapp http://localhost:5005 usuario: data pass: science, luego ir al menu Predecir y cargar los datos ej:

![alt text](predecir.png)

* Via Postman se puede consultar la API por ejemplo con los siguientes datos:
		
		POST / http://localhost:9000/predict_asistencia 
		
		body:
		{	
		 "DiasSemana": 1, 
		 "NroMes": 1,
		 "delta_idas": 1,
		 "edad": 34, 
		 "prom_asistencia": 0.766667,
		 "Sexo": "F"
		}
	

![alt text](postman.png)

