FROM python:2-slim-stretch
ADD . /
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
WORKDIR /
ENTRYPOINT python run.py
