URL_POST_PREDICT = 'http://predict_asistencia:9000/predict_asistencia'
SECRET_KEY_API = 'tcku8xwn9aK4MaePC76NnqpioZ22kY'


class Config(object):
    """
    Common configurations
    """

    # Put any configurations here that are common across all environments


class DevelopmentConfig(Config):
    """
    Development configurations
    """

    DEBUG = True
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}


