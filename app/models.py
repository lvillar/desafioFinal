from flask_login import UserMixin
from app import db, login_manager
import urllib2
import json
from config import *


class PredecirAsistencia(object):

    def __init__(self):
        self.prediccion_asistencia = 0
        self.score = 0

    def get(self, form):

        data = {"DiasSemana": form.diasSemana.data,
                     "NroMes": form.nroMes.data,
                     "delta_idas": form.deltaDias.data,
                     "edad": form.edad.data,
                     "prom_asistencia": form.promAsistencia.data,
                     "Sexo": str(form.sexo.data)}

        req = urllib2.Request(URL_POST_PREDICT)
        req.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(req, json.dumps(data))

        if response.code == 200:
            prediccion = json.load(response)
            self.prediccion_asistencia = prediccion['prediccion_asistencia']
            self.score = prediccion['score']
            return True
        else:
            return False


class Employee(UserMixin, db.Model):
    __tablename__ = 'employees'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), index=True, unique=True)
    keyapi = ''
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        self.password_hash = password

    def verify_password(self, password):
        return self.password_hash == password

    def __repr__(self):
        return '<Employee: {}>'.format(self.username)


# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return Employee.query.get(int(user_id))


