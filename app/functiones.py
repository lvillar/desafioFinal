# -*- coding: utf-8 -*-
from config import *
import httplib
from base64 import b64encode
import json


def alive_MQ():
    try:
        conn = httplib.HTTPConnection(RABBIT_NAME + ':' + RABBIT_PORT)
        user_pass = str(b64encode(b"" + usr_pws_rb_mq).decode("ascii"))
        headers = {"Authorization": "Basic " + user_pass}
        conn.request('GET', RABBIT_URL_ALIVE, headers=headers)
        r1 = conn.getresponse()
        respBody = r1.read()
        conn.close()
        resp_dict = json.loads(respBody)
        if str(resp_dict['status']) == 'ok':
            return True
        else:
            print (resp_dict)
            return False
    except Exception as e:
        print(str(e))
        return False


def publish_MQ(json_data, tipo_notificacion, queue_name):
    try:
        # SACADO DATA QUE NO ME INTERESA PARA LA NOTIFICACION
        if json_data.has_key('registrado_on'):
            del json_data['registrado_on']
        if json_data.has_key('confirmado_on'):
            del json_data['confirmado_on']

        # AGREGO DATA QUE SI ME INTERESA PARA EL SERVICIO DE NOTIFICACION
        if tipo_notificacion is not 'up_paciente':
            json_data['tipo_notificacion'] = tipo_notificacion

        str_data = str(json_data)
        conn = httplib.HTTPConnection(RABBIT_NAME + ':' + RABBIT_PORT)
        user_pass = str(b64encode(b"" + usr_pws_rb_mq).decode("ascii"))
        headers = {"Authorization": "Basic " + user_pass, "Content-type": "application/json",
                   "Accept": "application/json"}
        # payload = json_data
        payload = str_data
        body = {'properties': {'delivery_mode': 2}, 'routing_key': queue_name, 'payload': payload,
                'payload_encoding': 'string'}
        conn.request('POST', RABBIT_URL_PUB, headers=headers, body=json.dumps(body))
        r1 = conn.getresponse()
        respBody = r1.read()
        conn.close()
        resp_dict = json.loads(respBody)

        if r1.status == 200 and resp_dict['routed']:
            # logging.debug('publish_MQ --> resp: ' + str(resp_dict) + ' payload: ' + payload)
            # SACO EL CAMPO QUE LE AGREGO AL COMIENZO ASI NO LO TRASLADO AL GUARDADO DEL REGISTRO.
            if json_data.has_key("tipo_notificacion"):
                del json_data['tipo_notificacion']
            return True
        else:
            print(resp_dict)
            return False

    except Exception as e:
        print( str(e))
        return False
