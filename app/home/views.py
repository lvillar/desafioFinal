from flask import flash, render_template, abort
from flask_login import login_required, current_user
from forms import FormPredecir
from ..models import PredecirAsistencia
from . import home


def check_admin():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)


@home.route('/')
def homepage():
    return render_template('home/index.html', title="Desafio Final")


@home.route('/predecir', methods=['GET', 'POST'])
@login_required
def patient():
    form = FormPredecir()
    if form.validate_on_submit():
        # register = RegisterPortal()
        # if register.get(form.nrohc.data):
        register = PredecirAsistencia()
        if register.get(form):
            if register.prediccion_asistencia == 1:
                result = 'Asistira'
            else:
                result = 'NO Asistira'
            flash('El paciente:' + result , 'warning')
            flash('Metrica prediccion:' + str(register.score), 'warning')
            #return redirect(url_for('home.resultado', nrohc=register.nrohc))
        else:
            flash('Sevicio de prediccion no disponible.')
    return render_template('home/predecir.html', form=form, title="Desarfio Final")

