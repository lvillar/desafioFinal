from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError, validators, FieldList, SelectField, \
    IntegerField, FloatField
from wtforms.validators import DataRequired, Email, EqualTo


class FormPredecir(FlaskForm):
    diasSemana = SelectField('Dia de Semana',
                             choices=[(1, 'Lunes'), (2, 'Martes'), (3, 'Miercoles'), (4, 'Jueves'), (5, 'Viernes'),
                                      (6, 'Sabado')], coerce=int)
    nroMes = SelectField('Mes',
                             choices=[(1, 'Enero'), (2, 'Febrero'), (3, 'Marzo'), (4, 'Abril'), (5, 'Mayo'),
                                      (6, 'Junio'), (7, 'Julio'), (8, 'Agosto'), (9, 'Septiembre'), (10, 'Octubre'),
                                      (11, 'Noviembre'), (12, 'Diciembre')], coerce=int)

    deltaDias = IntegerField('Cantidad dias espera al turno', validators=[DataRequired()])
    edad = IntegerField('Edad del paciente', validators=[DataRequired()])
    promAsistencia = FloatField('Promedio Asistencia', validators=[DataRequired()])
    sexo = SelectField(u'Sexo', choices=[('F', 'Femenino'), ('M', 'Masculino')])

    submit = SubmitField('Predecir')



