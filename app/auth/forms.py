from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo

from config import SECRET_KEY_API
from ..models import Employee


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    keyapi = PasswordField('Key API', validators=[DataRequired()])
    password = PasswordField('Password', validators=[
                                        DataRequired(),
                                        EqualTo('confirm_password')
                                        ])
    confirm_password = PasswordField('Confirmar Password')
    submit = SubmitField('Registrarse')

    def validate_email(self, field):
        if Employee.query.filter_by(email=field.data).first():
            raise ValidationError('Email esta en uso.')

    def validate_username(self, field):
        if Employee.query.filter_by(username=field.data).first():
            raise ValidationError('Username esta en uso.')

    def validate_keyapi(self, field):
        if field.data != SECRET_KEY_API:
            raise ValidationError('Debe cargar una clave correcta')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
